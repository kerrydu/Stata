
	clear 
	set more off
	
	cap mata mata drop isocode()
	mata: 
	string matrix isocode(string scalar text){
	        
			file=cat(text)
			flag=strpos(file,"</td>")
			flag=(flag:>=1)
			file=select(file,flag)
			if (rows(file)==0){
			exit( )
			}
			
			return(file)
		   }
		end

	//mata:file=isocode("http://doc.chacuo.net//iso-3166-1")
	/*
	replace v=subinstr(v,"<td>","",.)
	replace v=subinstr(v,"</td>","",.)
	gen j=mod(_n,9)
	replace j=9 if j==0
	gen i=ceil(_n/9)
	*/
	*set obs 3
	//getmata (v)=file
	
	clear 
	set more off
	
	cap mata mata drop isocode()
	mata: 
	string matrix isocode(string scalar text){
	        
			file=cat(text)
			flag=strpos(file,`"label-module="para">"')
			flag=(flag:>=1)
			file=select(file,flag)
			if (rows(file)==0){
			exit( )
			}
			//i=(strpos(file,"</td></tr><tr><td>"):>1)
			//i=(i:>=1)
			//file=(file,i)
			//file=substr(file,strpos(file,`"label-module="para">"'),(strpos(file,"</div>")-strpos(file,`"label-module="para">"')))
			return(file)
		   }
		end
	
	
	mata:file=isocode("https://baike.baidu.com/item/ISO%203166-1/5269555?fr=aladdin")
	clear
	getmata (v)=file

	drop if _n<24
	
	gen i=(strpos(v,"></tr><tr><td>")>=1)
	replace i=i+i[_n-1]  if _n>1
	bys i: gen j=_n
	replace v=substr(v,strpos(v,`"para">"'),strpos(v,`"</div>"')-strpos(v,`"para">"'))
	replace v=subinstr(v,`"para">"',"",.)
	drop if j>6
	compress
	reshape wide v, i(i)  j(j)
	label var v1 "二位字母"
	label var v2 "三位字母"
	label var v3 "数字"
	label var v4 "ISO 3166-2相应代码"
	label var v5 "国家或地区（ISO 英文用名）"
	label var v6 "中国 惯用名"
	compress
	
	rename v2 countrycode
	rename v5 country
	rename v6 country_CHN
	rename v1 iso2code
	replace country_CHN=subinstr(country_CHN,"；","",.)
	replace country_CHN=subinstr(country_CHN," ","",.)
	save countrylist,replace
